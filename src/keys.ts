export enum keys {
    Alibaba = 'alibaba',
    AWS = 'aws',
    Azure = 'azure',
    DO = 'do',
    GCP = 'gcp',
    Oracle = 'oracle',
    Unknown = 'unknown'
}
