import {IncomingMessage, OutgoingHttpHeaders, request} from 'http';

enum HttpState {
    waiting,
    failed,
    success
}

export default class Request {
    private buffer: string = '';

    private state: HttpState = HttpState.waiting;

    private res: IncomingMessage;

    public get statusCode(): number {
        try {
            return this.res.statusCode;
        } catch(e) {
            return 0;
        }
    }

    public constructor(url: string, headers: OutgoingHttpHeaders = {}, options: { timeout: number } = {timeout: 3000}) {
        let timeoutRef: any;
        const req = request(url, {
            headers
        }, res => {
            this.res = res;
            res.setEncoding('utf8');
            res.on('data', chunk => this.buffer += chunk);
            res.on('end', () => {
                clearTimeout(timeoutRef);
                this.state = HttpState.success;
            });
        }).on('error', () => this.state = HttpState.failed)
            .on('abort', () => this.state = HttpState.failed);

        req.end();

        timeoutRef = setTimeout(req.abort.bind(req), options.timeout);
    }

    /**
     * Will resolve with the parsed JSON, or reject if either the request or the parsing failed.
     *
     * @returns {Promise<void>}
     */
    public async json() {
        return await JSON.parse(await this.body());
    }

    /**
     * Resolved with body when the request is done, or reject if it failed.
     *
     * @returns {Promise<string>}
     */
    public body(): Promise<string> {
        const retry = (check: () => boolean, cont: () => void): any => check() ? cont() : setTimeout(() => retry(check, cont), 1000);

        return new Promise((resolve, reject) => {
            retry(() => this.state !== HttpState.waiting, () => {
                if (this.state === HttpState.failed) {
                    return reject(new Error('HTTP request failed'));
                }

                resolve(this.buffer);
            });
        });
    }
}
