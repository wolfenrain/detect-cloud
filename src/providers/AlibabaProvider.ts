import {existsSync, readFileSync} from 'fs';
import {IProvider}                from '../IProvider';
import {keys}                     from '../keys';
import Request                    from '../Request';

export class AlibabaProvider implements IProvider {
    public static get key() {
        return keys.Alibaba;
    }

    private static metadataUrl: string = 'http://100.100.100.200/latest/meta-data/instance/instance-type';

    private static vendorFile: string = '/sys/class/dmi/id/product_name';

    public async identify() {
        return (await this.checkMetaServer()) || (await this.checkVendorFile()) ? AlibabaProvider.key : keys.Unknown;
    }

    public async checkMetaServer() {
        const req = new Request(AlibabaProvider.metadataUrl);

        try {
            const result = await req.body();
            return req.statusCode === 200 && result.startsWith('ecs.');
        } catch (e) {
            return false;
        }
    }

    public async checkVendorFile() {
        if (existsSync(AlibabaProvider.vendorFile)) {
            if (~readFileSync(AlibabaProvider.vendorFile, 'utf8').indexOf('Alibaba Cloud')) {
                return true;
            }
        }
        return false;
    }
}
