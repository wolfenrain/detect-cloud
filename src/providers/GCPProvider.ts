import {existsSync, readFileSync} from 'fs';
import {IProvider}                from '../IProvider';
import {keys}                     from '../keys';
import Request                    from '../Request';

export class GCPProvider implements IProvider {
    public static get key() {
        return keys.GCP;
    }

    private static metadataUrl: string = 'http://metadata.google.internal/computeMetadata/v1/instance/tags';

    private static vendorFile: string = '/sys/class/dmi/id/product_name';

    private static headers: any = {'Metadata-Flavor': 'Google'};

    public async identify() {
        return (await this.checkMetaServer()) || (await this.checkVendorFile()) ? GCPProvider.key : keys.Unknown;
    }

    public async checkMetaServer() {
        const req = new Request(GCPProvider.metadataUrl, GCPProvider.headers);

        try {
            await req.body();
            return req.statusCode === 200;
        } catch (e) {
            return false;
        }
    }

    public async checkVendorFile() {
        if (existsSync(GCPProvider.vendorFile)) {
            if (~readFileSync(GCPProvider.vendorFile, 'utf8').indexOf('Google')) {
                return true;
            }
        }
        return false;
    }
}
