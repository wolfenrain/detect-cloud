import {existsSync, readFileSync} from 'fs';
import {IProvider}                from '../IProvider';
import {keys}                     from '../keys';
import Request                    from '../Request';

export class OracleProvider implements IProvider {
    public static get key() {
        return keys.Oracle;
    }

    private static metadataUrl: string = 'http://169.254.169.254/opc/v1/instance/metadata/';

    private static vendorFile: string = '/sys/class/dmi/id/chassis_asset_tag';

    public async identify() {
        return (await this.checkMetaServer()) || (await this.checkVendorFile()) ? OracleProvider.key : keys.Unknown;
    }

    public async checkMetaServer() {
        const req = new Request(OracleProvider.metadataUrl);

        try {
            const result = await req.json();
            return req.statusCode === 200 && !!~result.OkeTM.indexOf('oke');
        } catch (e) {
            return false;
        }
    }

    public async checkVendorFile() {
        if (existsSync(OracleProvider.vendorFile)) {
            if (~readFileSync(OracleProvider.vendorFile, 'utf8').indexOf('OracleCloud')) {
                return true;
            }
        }
        return false;
    }
}
