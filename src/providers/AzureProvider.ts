import {existsSync, readFileSync} from 'fs';
import {IProvider}                from '../IProvider';
import {keys}                     from '../keys';
import Request                    from '../Request';

export class AzureProvider implements IProvider {
    public static get key() {
        return keys.Azure;
    }

    private static metadataUrl: string = 'http://169.254.169.254/metadata/instance?api-version=2017-12-01';

    private static vendorFile: string = '/sys/class/dmi/id/sys_vendor';

    private static headers: any = {Metadata: 'true'};

    public async identify() {
        return (await this.checkMetaServer()) || (await this.checkVendorFile()) ? AzureProvider.key : keys.Unknown;
    }

    public async checkMetaServer() {
        const req = new Request(AzureProvider.metadataUrl, AzureProvider.headers);

        try {
            await req.body();
            return req.statusCode === 200;
        } catch (e) {
            return false;
        }
    }

    public async checkVendorFile() {
        if (existsSync(AzureProvider.vendorFile)) {
            if (~readFileSync(AzureProvider.vendorFile, 'utf8').indexOf('Microsoft Corporation')) {
                return true;
            }
        }
        return false;
    }
}
