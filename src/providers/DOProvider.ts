import {existsSync, readFileSync} from 'fs';
import {IProvider}                from '../IProvider';
import {keys}                     from '../keys';
import Request                    from '../Request';

export class DOProvider implements IProvider {
    public static get key() {
        return keys.DO;
    }

    private static metadataUrl: string = 'http://169.254.169.254/metadata/v1.json';

    private static vendorFile: string = '/sys/class/dmi/id/sys_vendor';

    public async identify() {
        return (await this.checkMetaServer()) || (await this.checkVendorFile()) ? DOProvider.key : keys.Unknown;
    }

    public async checkMetaServer() {
        const req = new Request(DOProvider.metadataUrl);

        try {
            const result = await req.json();
            return req.statusCode === 200 && result.droplet_id > 0;
        } catch (e) {
            return false;
        }
    }

    public async checkVendorFile() {
        if (existsSync(DOProvider.vendorFile)) {
            if (~readFileSync(DOProvider.vendorFile, 'utf8').indexOf('DigitalOcean')) {
                return true;
            }
        }
        return false;
    }
}
