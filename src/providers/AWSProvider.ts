import {existsSync, readFileSync} from 'fs';
import {IProvider}                from '../IProvider';
import {keys}                     from '../keys';
import Request                    from '../Request';

export class AWSProvider implements IProvider {
    public static get key() {
        return keys.AWS;
    }

    private static metadataUrl: string = 'http://169.254.169.254/latest/dynamic/instance-identity/document';

    private static vendorFile: string = '/sys/class/dmi/id/product_version';

    public async identify() {
        return (await this.checkMetaServer()) || (await this.checkVendorFile()) ? AWSProvider.key : keys.Unknown;
    }

    public async checkMetaServer() {
        const req = new Request(AWSProvider.metadataUrl);

        try {
            const result = await req.json();
            return req.statusCode === 200 && result.ImageId.startsWith('ami-') && result.InstanceID.startsWith('i-');
        } catch (e) {
            return false;
        }
    }

    public async checkVendorFile() {
        if (existsSync(AWSProvider.vendorFile)) {
            if (~readFileSync(AWSProvider.vendorFile, 'utf8').indexOf('amazon')) {
                return true;
            }
        }
        return false;
    }
}
