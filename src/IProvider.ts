import {keys} from './keys';

export interface IProvider {
    identify(): Promise<keys>;

    checkMetaServer(): Promise<boolean>;

    checkVendorFile(): Promise<boolean>;
}
