import {IProvider}       from './src/IProvider';
import {keys}            from './src/keys';
import {AlibabaProvider} from './src/providers/AlibabaProvider';
import {AWSProvider}     from './src/providers/AWSProvider';
import {AzureProvider}   from './src/providers/AzureProvider';
import {DOProvider}      from './src/providers/DOProvider';
import {GCPProvider}     from './src/providers/GCPProvider';
import {OracleProvider}  from './src/providers/OracleProvider';

const providers: Array<(new () => IProvider)> = [
    AlibabaProvider,
    AWSProvider,
    AzureProvider,
    DOProvider,
    GCPProvider,
    OracleProvider
];

const detectCloud = async (...excludes: keys[]) => {
    for (const providerClass of providers) {
        if (excludes.indexOf((<any>providerClass).key)) {
            const identifiedProvider = await (new providerClass()).identify();
            if (identifiedProvider !== keys.Unknown) {
                return identifiedProvider;
            }
        }
    }
    return keys.Unknown;
};

export default detectCloud;
export {keys}          from './src/keys';
