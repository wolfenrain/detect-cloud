import * as nock from 'nock';
import Request   from '../../src/Request';

describe('Request', () => {
    describe('.constructor()', () => {
        it('', async done => {
            const body = {
                results: [
                    {
                        test: 1
                    }
                ]
            };

            (<any>nock)('http://unit.test').get('/test').reply(200, body);

            const req = new Request('http://unit.test/test');

            expect(await req.body()).toEqual(JSON.stringify(body));
            done();
        });
    });

    describe('.statusCode', () => {
        it('should return the statusCode on a success full request', async done => {
            const statusCode = 200;
            (<any>nock)('http://unit.test').get('/test').reply(statusCode, {});

            const req = new Request('http://unit.test/test');
            await req.body();

            expect(req.statusCode).toEqual(statusCode);
            done();
        });

        it('should return 0 when request failed', async done => {
            const req = new Request('http://unit.test/test');
            try {
                await req.body();
            } catch (e) {
                expect(req.statusCode).toEqual(0);
                done();
            }
        });
    });

    describe('.json()', () => {
        it('should return a JSON object', async done => {
            const body = {
                results: [
                    {
                        test: 1
                    }
                ]
            };

            (<any>nock)('http://unit.test').get('/test').reply(200, body);

            const req = new Request('http://unit.test/test');

            expect(await req.json()).toEqual(body);
            done();
        });
    });

    describe('.body()', () => {
        it('should return a stringified JSON object', async done => {
            const body = {
                results: [
                    {
                        test: 1
                    }
                ]
            };

            (<any>nock)('http://unit.test').get('/test').reply(200, body);

            const req = new Request('http://unit.test/test');

            expect(await req.body()).toEqual(JSON.stringify(body));
            done();
        });
    });
})
;
