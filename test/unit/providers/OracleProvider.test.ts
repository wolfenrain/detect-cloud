import {keys}           from '../../../src/keys';
import {AWSProvider}    from '../../../src/providers/AWSProvider';
import {OracleProvider} from '../../../src/providers/OracleProvider';
import Request          from '../../../src/Request';

describe('OracleProvider', () => {
    beforeEach(() => {
        (<any> OracleProvider).vendorFile = './test/vendor_files/oracle';
    });

    describe('.key', () => {
        it('should return "keys.Oracle"', () => {
            expect(OracleProvider.key).toEqual(keys.Oracle);
        });
    });

    describe('.identify()', () => {
        it('should return the provider key based on metaserver', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({
                OkeTM: 'oke'
            }));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new OracleProvider();

            expect(await provider.identify()).toEqual(OracleProvider.key);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return the provider key based on filesystem', async done => {
            const provider = new OracleProvider();

            expect(await provider.identify()).toEqual(OracleProvider.key);
            done();
        });

        it('should return the unknown key if no technique works', async done => {
            (<any> OracleProvider).vendorFile = './test/vendor_files/unknown';

            const provider = new OracleProvider();

            expect(await provider.identify()).toEqual(keys.Unknown);
            done();
        });
    });

    describe('.checkMetaServer()', () => {
        it('should return true if server was reached', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({
                OkeTM: 'oke'
            }));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new OracleProvider();

            expect(await provider.checkMetaServer()).toEqual(true);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return false if the server was not reached', async done => {
            const provider = new OracleProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            done();
        });

        it('should return false if there is no "OkeTM"', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({}));

            const provider = new AWSProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            expect(bodySpy).toBeCalledTimes(1);
            done();
        });
    });

    describe('.checkVendorFile()', () => {
        it('should return true with the correct vendor file', async done => {
            const provider = new OracleProvider();

            expect(await provider.checkVendorFile()).toEqual(true);
            done();
        });

        it('should return false with the incorrect vendor file', async done => {
            (<any> OracleProvider).vendorFile = './test/vendor_files/do';
            const provider = new OracleProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });

        it('should return false when the vendor file is not found', async done => {
            (<any> OracleProvider).vendorFile = './test/vendor_files/unknown';
            const provider = new OracleProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });
    });
});
