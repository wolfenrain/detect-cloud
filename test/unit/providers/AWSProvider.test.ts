import {keys}        from '../../../src/keys';
import {AWSProvider} from '../../../src/providers/AWSProvider';
import Request       from '../../../src/Request';

describe('AWSProvider', () => {
    beforeEach(() => {
        (<any> AWSProvider).vendorFile = './test/vendor_files/aws';
    });

    describe('.key', () => {
        it('should return "keys.AWS"', () => {
            expect(AWSProvider.key).toEqual(keys.AWS);
        });
    });

    describe('.identify()', () => {
        it('should return the provider key based on metaserver', async done => {
            const spy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({
                ImageId: 'ami-' + Math.random(),
                InstanceID: 'i-' + Math.random()
            }));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AWSProvider();

            expect(await provider.identify()).toEqual(AWSProvider.key);
            expect(spy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return the provider key based on filesystem', async done => {
            const provider = new AWSProvider();

            expect(await provider.identify()).toEqual(AWSProvider.key);
            done();
        });

        it('should return the unknown key if no technique works', async done => {
            (<any> AWSProvider).vendorFile = './test/vendor_files/unknown';

            const provider = new AWSProvider();

            expect(await provider.identify()).toEqual(keys.Unknown);
            done();
        });
    });

    describe('.checkMetaServer()', () => {
        it('should return true if all keys exists', async done => {
            const spy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({
                ImageId: 'ami-' + Math.random(),
                InstanceID: 'i-' + Math.random()
            }));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AWSProvider();

            expect(await provider.checkMetaServer()).toEqual(true);
            expect(spy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return false if the server was not reached', async done => {
            const provider = new AWSProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            done();
        });

        it('should return false if there is no "ImageId"', async done => {
            const spy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({
                InstanceID: 'i-' + Math.random()
            }));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AWSProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            expect(spy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return false if there is no "InstanceID"', async done => {
            const spy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({
                ImageId: 'ami-' + Math.random()
            }));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AWSProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            expect(spy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return false if no JSON was returned', async done => {
            const spy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify(''));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AWSProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            expect(spy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });
    });

    describe('.checkVendorFile()', () => {
        it('should return true with the correct vendor file', async done => {
            const provider = new AWSProvider();

            expect(await provider.checkVendorFile()).toEqual(true);
            done();
        });

        it('should return false with the incorrect vendor file', async done => {
            (<any> AWSProvider).vendorFile = './test/vendor_files/do';
            const provider = new AWSProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });

        it('should return false when the vendor file is not found', async done => {
            (<any> AWSProvider).vendorFile = './test/vendor_files/unknown';
            const provider = new AWSProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });
    });
});
