import {keys}        from '../../../src/keys';
import {GCPProvider} from '../../../src/providers/GCPProvider';
import Request       from '../../../src/Request';

describe('GCPProvider', () => {
    beforeEach(() => {
        (<any> GCPProvider).vendorFile = './test/vendor_files/gcp';
    });

    describe('.key', () => {
        it('should return "keys.GCP"', () => {
            expect(GCPProvider.key).toEqual(keys.GCP);
        });
    });

    describe('.identify()', () => {
        it('should return the provider key based on metaserver', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => '');
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new GCPProvider();

            expect(await provider.identify()).toEqual(GCPProvider.key);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return the provider key based on filesystem', async done => {
            const provider = new GCPProvider();

            expect(await provider.identify()).toEqual(GCPProvider.key);
            done();
        });

        it('should return the unknown key if no technique works', async done => {
            (<any> GCPProvider).vendorFile = './test/vendor_files/unknown';

            const provider = new GCPProvider();

            expect(await provider.identify()).toEqual(keys.Unknown);
            done();
        });
    });

    describe('.checkMetaServer()', () => {
        it('should return true if server was reached', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => '');
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new GCPProvider();

            expect(await provider.checkMetaServer()).toEqual(true);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return false if the server was not reached', async done => {
            const provider = new GCPProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            done();
        });
    });

    describe('.checkVendorFile()', () => {
        it('should return true with the correct vendor file', async done => {
            const provider = new GCPProvider();

            expect(await provider.checkVendorFile()).toEqual(true);
            done();
        });

        it('should return false with the incorrect vendor file', async done => {
            (<any> GCPProvider).vendorFile = './test/vendor_files/do';
            const provider = new GCPProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });

        it('should return false when the vendor file is not found', async done => {
            (<any> GCPProvider).vendorFile = './test/vendor_files/unknown';
            const provider = new GCPProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });
    });
});
