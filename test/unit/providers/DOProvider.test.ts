import {keys}       from '../../../src/keys';
import {DOProvider} from '../../../src/providers/DOProvider';
import Request      from '../../../src/Request';

describe('DOProvider', () => {
    beforeEach(() => {
        (<any> DOProvider).vendorFile = './test/vendor_files/do';
    });

    describe('.key', () => {
        it('should return "keys.DO"', () => {
            expect(DOProvider.key).toEqual(keys.DO);
        });
    });

    describe('.identify()', () => {
        it('should return the provider key based on metaserver', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({droplet_id: 1}));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new DOProvider();

            expect(await provider.identify()).toEqual(DOProvider.key);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return the provider key based on filesystem', async done => {
            const provider = new DOProvider();

            expect(await provider.identify()).toEqual(DOProvider.key);
            done();
        });

        it('should return the unknown key if no technique works', async done => {
            (<any> DOProvider).vendorFile = './test/vendor_files/unknown';

            const provider = new DOProvider();

            expect(await provider.identify()).toEqual(keys.Unknown);
            done();
        });
    });

    describe('.checkMetaServer()', () => {
        it('should return true if there is a "droplet_id"', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({droplet_id: 1}));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new DOProvider();

            expect(await provider.checkMetaServer()).toEqual(true);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return false if the server was not reached', async done => {
            const provider = new DOProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            done();
        });

        it('should return false if there is no "droplet_id"', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify({}));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new DOProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return false if no JSON was returned', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => JSON.stringify(''));
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new DOProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });
    });

    describe('.checkVendorFile()', () => {
        it('should return true with the correct vendor file', async done => {
            const provider = new DOProvider();

            expect(await provider.checkVendorFile()).toEqual(true);
            done();
        });

        it('should return false with the incorrect vendor file', async done => {
            (<any> DOProvider).vendorFile = './test/vendor_files/aws';
            const provider = new DOProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });

        it('should return false when the vendor file is not found', async done => {
            (<any> DOProvider).vendorFile = './test/vendor_files/unknown';
            const provider = new DOProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });
    });
});
