import {keys}            from '../../../src/keys';
import {AlibabaProvider} from '../../../src/providers/AlibabaProvider';
import Request           from '../../../src/Request';

describe('AlibabaPRovider', () => {
    beforeEach(() => {
        (<any> AlibabaProvider).vendorFile = './test/vendor_files/alibaba';
    });

    describe('.key', () => {
        it('should return "keys.Alibaba"', () => {
            expect(AlibabaProvider.key).toEqual(keys.Alibaba);
        });
    });

    describe('.identify()', () => {
        it('should return the provider key based on metaserver', async done => {
            const spy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => 'ecs.' + Math.random());
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AlibabaProvider();

            expect(await provider.identify()).toEqual(AlibabaProvider.key);
            expect(spy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return the provider key based on filesystem', async done => {
            const provider = new AlibabaProvider();

            expect(await provider.identify()).toEqual(AlibabaProvider.key);
            done();
        });

        it('should return the unknown key if no technique works', async done => {
            (<any> AlibabaProvider).vendorFile = './test/vendor_files/unknown';

            const provider = new AlibabaProvider();

            expect(await provider.identify()).toEqual(keys.Unknown);
            done();
        });
    });

    describe('.checkMetaServer()', () => {
        it('should return true if all keys exists', async done => {
            const spy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => 'ecs.' + Math.random());
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AlibabaProvider();

            expect(await provider.checkMetaServer()).toEqual(true);
            expect(spy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return false if the server was not reached', async done => {
            const provider = new AlibabaProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            done();
        });

        it('should return false if the body does not start with "ecs."', async done => {
            const spy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => 'noo.' + Math.random());
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AlibabaProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            expect(spy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });
    });

    describe('.checkVendorFile()', () => {
        it('should return true with the correct vendor file', async done => {
            const provider = new AlibabaProvider();

            expect(await provider.checkVendorFile()).toEqual(true);
            done();
        });

        it('should return false with the incorrect vendor file', async done => {
            (<any> AlibabaProvider).vendorFile = './test/vendor_files/do';
            const provider = new AlibabaProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });

        it('should return false when the vendor file is not found', async done => {
            (<any> AlibabaProvider).vendorFile = './test/vendor_files/unknown';
            const provider = new AlibabaProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });
    });
});
