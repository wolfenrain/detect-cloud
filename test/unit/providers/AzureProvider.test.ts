import {keys}          from '../../../src/keys';
import {AzureProvider} from '../../../src/providers/AzureProvider';
import Request         from '../../../src/Request';

describe('AzureProvider', () => {
    beforeEach(() => {
        (<any> AzureProvider).vendorFile = './test/vendor_files/azure';
    });

    describe('.key', () => {
        it('should return "keys.Azure"', () => {
            expect(AzureProvider.key).toEqual(keys.Azure);
        });
    });

    describe('.identify()', () => {
        it('should return the provider key based on metaserver', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => '');
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AzureProvider();

            expect(await provider.identify()).toEqual(AzureProvider.key);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return the provider key based on filesystem', async done => {
            const provider = new AzureProvider();

            expect(await provider.identify()).toEqual(AzureProvider.key);
            done();
        });

        it('should return the unknown key if no technique works', async done => {
            (<any> AzureProvider).vendorFile = './test/vendor_files/unknown';

            const provider = new AzureProvider();

            expect(await provider.identify()).toEqual(keys.Unknown);
            done();
        });
    });

    describe('.checkMetaServer()', () => {
        it('should return true if server was reached', async done => {
            const bodySpy = jest.spyOn(Request.prototype, 'body').mockImplementationOnce(async () => '');
            const statusSpy = jest.spyOn(Request.prototype, 'statusCode', 'get').mockImplementationOnce(() => 200);

            const provider = new AzureProvider();

            expect(await provider.checkMetaServer()).toEqual(true);
            expect(bodySpy).toBeCalledTimes(1);
            expect(statusSpy).toBeCalledTimes(1);
            done();
        });

        it('should return false if the server was not reached', async done => {
            const provider = new AzureProvider();

            expect(await provider.checkMetaServer()).toEqual(false);
            done();
        });
    });

    describe('.checkVendorFile()', () => {
        it('should return true with the correct vendor file', async done => {
            const provider = new AzureProvider();

            expect(await provider.checkVendorFile()).toEqual(true);
            done();
        });

        it('should return false with the incorrect vendor file', async done => {
            (<any> AzureProvider).vendorFile = './test/vendor_files/do';
            const provider = new AzureProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });

        it('should return false when the vendor file is not found', async done => {
            (<any> AzureProvider).vendorFile = './test/vendor_files/unknown';
            const provider = new AzureProvider();

            expect(await provider.checkVendorFile()).toEqual(false);
            done();
        });
    });
});
