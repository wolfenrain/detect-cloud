[![pipeline status](https://gitlab.com/wolfenrain/detect-cloud/badges/master/pipeline.svg)](https://gitlab.com/wolfenrain/detect-cloud/pipelines)
[![coverage report](https://gitlab.com/wolfenrain/detect-cloud/badges/master/coverage.svg)](https://gitlab.com/wolfenrain/detect-cloud)
[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)
[![npm version](http://img.shields.io/npm/v/detect-cloud.svg?style=flat)](https://npmjs.org/package/detect-cloud "View this project on npm")

# detect-cloud

This node module is inspired by the golang library [Satellite](https://github.com/banzaicloud/satellite). Most of the code is based on the same techniques used by Satellite.  

## Supported Cloud Providers
- Alibaba
- Amazon
- Azure
- DigitialOcean
- Google Cloud
- Oracle

## Usage

It will check for every supported cloud provider by default:

```javascript
import detectCloud, {keys} from 'detect-cloud';

const provider = await detectCloud();
switch (provider) {
    case keys.AWS:
        // AWS related code.
        break;
    case keys.Azure:
        // Azure related code.
        break;
    ...  
}
```

Or if you wont support certain clouds, or want to skip that:
```javascript
import detectCloud, {keys} from 'detect-cloud';

const provider = await detectCloud(keys.GCP, keys.Azure);
switch (provider) {
    case keys.AWS:
        // AWS related code.
        break;
    case keys.DO:
        // DO related code.
        break;
    ...  
}
```

## Contributing

If you find this project useful here's how you can help:

- Check if your bug or feature was already reported
- Create a merge request with your changes
  - Make sure the tests are also working and updated