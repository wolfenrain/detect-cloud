/label ~"type:feature"
### Probleem om op te lossen
<!-- Beschrijf het probleem dat er momenteel bestaat -->

### Motivering / use-cases
<!-- Waarom is het belangrijk dit probleem op te lossen -->

### Voorgestelde oplossing
<!-- Wat stel je voor om dit probleem op te lossen -->

### Plus- en minputen van de oplossing
<!-- Wat zijn de plus en minpunten mochten die er zijn -->
- [+ pluspunt 1 +]
- [+ pluspunt 2 +]
- [- minpunt 1 -]
- [- minpunt 1 -]

### Links/referenties
<!-- Zijn er links of referenties naar deze oplossingsrichting -->
* [Link 1](https//google.nl)
* [Link 2](https//mobilea.nl)