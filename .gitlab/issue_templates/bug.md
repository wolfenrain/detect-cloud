/label ~"type:bug"
### Versie
<!-- uit de package.json -->

### Huidige gedrag
<!-- Wat gebeurd er op dit moment? -->

### Verwachte gedrag
<!-- Wat had je verwacht dat er ging gebeuren? -->

### Stappen om te reproduceren
<!--
Probeer zoveel mogelijk de juiste stappen te noteren.
-->

### Log informatie

```
Voeg relevante log informatie hier toe
```

### Relevante code

```
Voeg relevante code hier toe
```

### Overige informatie
<!-- Nog tips? Links, afbeeldingen of logfiles of iets dergelijks of andere hulpmiddelen voor oplossen of verduidelijken van dit issue -->
